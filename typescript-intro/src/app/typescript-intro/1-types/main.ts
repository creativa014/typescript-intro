
enum GENDER {
  male = 'mr',
  female = 'ms'
}

enum PERSON_TYPE {
  employee,
  contact,
  customer
}

interface IBaseEntity {
  id: number;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  rowVersion: string;

  isItDeleted(): boolean;
}

abstract class BaseEntity implements IBaseEntity {
  
  isItDeleted(): boolean {
    return !(this.deletedAt == null);
  }

  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  id: number;
  rowVersion: string;
}

class Company extends BaseEntity {

  constructor(name: string, id: number) {
    super();
    this.id = id;
    this.name = name;
  }

  name: string;
}

class Person extends BaseEntity {
  name: string;
  age: number;
  gender: GENDER;
  type: PERSON_TYPE;
  company: Company;
}

const company = new Company('Lemi', 123);

/**
 * {
 *  name: 'Lemi',
 *  id: 123
 * }
 */

